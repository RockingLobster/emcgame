#include <iostream>

#include "Board.h"


int main()
{
	Piece piece(Piece::Colour::LIGHTO, Piece::Shape::CIRCLE, Piece::Body::HOLLOW, Piece::Height::TALL);
	std::cout << piece<<"\n";
	Board board;
	std::cout << board<<"\n\n\n\n";
	board[{1, 1}] = piece;
	std::cout << board;
	return 0;
}